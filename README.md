# Rust-Nix

This is a very basic `flake`-based setup for Rust projects on Nix.

## Steps:

1. Clone this project or copy the `flake.nix` file in the project
2. Add the `flake.nix` file to your Rust project's root directory
3. Remember to `git add flake.nix`
4. Run `nix develop`
